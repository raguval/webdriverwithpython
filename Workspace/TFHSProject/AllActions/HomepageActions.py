'''
Created on 22 Jan 2017

@author: Raghuv
'''
from selenium import webdriver
from selenium.webdriver import firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from AllPageObjects.HomePagePOs import HomepagePO
from AllActions.CommonActions import MyClass
 

class SpecificActs(object):
    '''
    classdocs
    '''


    def __init__(self, driver):
        '''
        Constructor
        '''
        self.driver=driver
        
        
        
    #when calling this function either pass the string "One-way" to click on the Oneway trip 
    #or
    #pass the string "Return" to click on the Return trip
    def SingleorRoundtrip(self,OnewayOrReturn):
        HPO = HomepagePO(self.driver)
        SingleorRoundtrip = HPO.OnewayorReturnloc(OnewayOrReturn)
        SingleorRoundtrip.click()
        
    
    
    def SelectCityAndAirport(self,FromOrTo,Cityname,AirportName):
        HPO = HomepagePO(self.driver)
        City=HPO.FromcityorTocityloc(FromOrTo)
        City.clear()
        City.click()
        City.send_keys(Cityname)
        Airportname = HPO.Airportname(AirportName)
        Airportname.click()
    
    #when calling this function either pass the string "Departdate" to select Depart date calendar icon 
    #or
    #pass the string "Returndate" to select Return date calendar icon
    def ClickonCalendarIcon(self,DepartdateOrReturndate):
        datedepart= "Departdate"
        returndate= "Returndate"
        if(datedepart is DepartdateOrReturndate):
            HPO = HomepagePO(self.driver)
            calendariconloc=HPO.CalendarIconLoc("Depart")
            calendariconloc.click()
        if(returndate is DepartdateOrReturndate):    
            calendariconloc = HPO.CalendarIconLoc("Return")
            calendariconloc.click()
            
    def selectdatefromdisplayedcalendar(self,travelday):
        mc= MyClass(self.driver)
        mc.Selectdate(travelday)
        
    def ClickonSearchButton(self):
        
        HPO = HomepagePO(self.driver)
        searchButton=HPO.SearchButtonloc()
        searchButton.click()
        
    def Displayallprices(self):
        mc= MyClass(self.driver)
        mc.storeallprices("//*[contains(text(),'View Deal')]/../../../../../..//div[contains(@class,'pricerange')]/a")
                
            
    
        
        
        
        
        
        
        
        
          