from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.webdriver import WebDriver
from AllActions.CommonActions import MyClass

'''
Created on 20 Jan 2017

@author: Raghuv
'''
class HomepagePO(object):
    
    
    
    
    def __init__(self, driver):
        self.driver = driver
      
    
    def flightloc(self):
        
        locatorstring = "bbbb"
        CA = MyClass(self.driver)
        CA.ElementwithFluentwait(locatorstring,10)
        flightloc =self.driver.find_element_by_xpath(locatorstring)        
        hl = self.driver.find
        return flightloc
    
    def OnewayorReturnloc(self, onewayorreturn):
        
        locatorstring = "//label/span[contains(text(),'"+onewayorreturn+"')]"
        CA = MyClass(self.driver)
        CA.ElementwithFluentwait(locatorstring, 30)
        Onewayloc =self.driver.find_element_by_xpath(locatorstring)
        return Onewayloc
    
    def FromcityorTocityloc(self,FromOrTo):
        
        '''locatorstring = "//div[contains(@class,'EditOrigin')]/..//input[contains(@placeholder,'"+FromOrTo+"')]" '''
        locatorstring = "//div/input[contains(@placeholder,'"+FromOrTo+"')]"
        CA = MyClass(self.driver)
        CA.ElementwithFluentwait(locatorstring,0)
        Fromcityloc =self.driver.find_element_by_xpath(locatorstring)
        return Fromcityloc
    
        
    def Airportname(self,AirportShortcode):
        
        locatorstring = "//li[contains(@id,'ap-"+AirportShortcode+"')]"
        CA = MyClass(self.driver)
        CA.ElementwithFluentwait(locatorstring,40)
        Airportname =self.driver.find_element_by_xpath(locatorstring)
        return Airportname
    
    def DepartdateCalendarIconLoc(self):
        
        locatorstring = "//div[contains(@class,'EditOrigin')]/..//div[3]//*[contains(@class,'icon-calendar')]"
        CA = MyClass(self.driver)
        CA.ElementwithFluentwait(locatorstring,40)
        calendarIconLoc =self.driver.find_element_by_xpath(locatorstring)
        return calendarIconLoc
    
    def SearchButtonloc(self):
        
        '''locatorstring = "//div[contains(@class,'EditOrigin')]/..//span[contains(text(),'Search')]/.."'''
        locatorstring = "//div[contains(@class,'search-form-grid-flights')]//button/span[contains(text(),'Search')]/.."
        
        CA = MyClass(self.driver)
        CA.ElementwithFluentwait(locatorstring,40)
        SearchButtonloc =self.driver.find_element_by_xpath(locatorstring)
        return SearchButtonloc
    
    def AllpricesLoc(self):
        locatorstring = "//span[contains(text(),'Search')]/.."
        CA = MyClass(self.driver)
        CA.ElementwithFluentwait(locatorstring,40)
        allpricesLoc =self.driver.find_element_by_xpath(locatorstring)
        return locatorstring

        
        
    # Pass the string "Depart" to select calendar icon of depart date 
    # or 
    #Pass the string "Return" to select the calendar icon of return date
    def CalendarIconLoc(self,DepartOrReturn):
        departicon = "Depart"
        returnicon = "Return"
        
        if(departicon is DepartOrReturn):
            
            locatorstring = "//input[contains(@ placeholder,'To')]/../../..//..//div[contains(@class,'col-depart-date')]//*[contains(@class,'icon-calendar-depart')]"
            CA = MyClass(self.driver)
            CA.ElementwithFluentwait(locatorstring,40)
            calendarIconLoc =self.driver.find_element_by_xpath(locatorstring)
            return calendarIconLoc
        if(returnicon is DepartOrReturn):
            locatorstring = "//input[contains(@ placeholder,'To')]/../../..//..//div[contains(@class,'col-return-date')]//*[contains(@class,'icon-calendar-depart')]"
            CA = MyClass(self.driver)
            CA.ElementwithFluentwait(locatorstring,40)
            calendarIconLoc =self.driver.find_element_by_xpath(locatorstring)
            return calendarIconLoc
    
            
            

        
            
        
    


    



    
    
        
        