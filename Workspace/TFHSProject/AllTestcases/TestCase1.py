'''
Created on 22 Jan 2017

@author: Raghuv
'''
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from AllPageObjects.HomePagePOs import HomepagePO
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from AllActions.HomepageActions import SpecificActs
from selenium.webdriver.common.keys import Keys

class Test(unittest.TestCase):


    def setUp(self):
        self.driver = webdriver.Firefox()
        '''self.driver = webdriver.Ie("C:\MiscellaneousSoftware\IEexecutable\IEDriverServer.exe")'''
        self.driver.get("https://www.kayak.co.uk/flights")
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        pass

    


    def testName(self):
        sa = SpecificActs(self.driver)
        sa.SingleorRoundtrip("One-way")
        sa.SelectCityAndAirport("From", "London", "LHR")
        sa.SelectCityAndAirport("To", "Madrid", "MAD")
        sa.ClickonCalendarIcon("Departdate")
        sa.selectdatefromdisplayedcalendar(25)
        sa.ClickonSearchButton()
        sa.Displayallprices()
        
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    